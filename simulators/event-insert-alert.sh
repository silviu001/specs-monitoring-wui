#!/bin/bash

if [ $# -lt 4 ];then
	echo "Usage: $0 EVENTHUB_IP_ADDRESS NUMBER_OF_MONITORING_AGENTS SIMULATOR_TYPE HOW_MANY_EVENTS [SIMULATOR_OPTIONS]"
	echo
	echo "    SIMULATOR_OPTIONS:"
	echo "          webpool:"
	echo "              SECMETRIC(M1_Redundancy|M2_Diversity) SECMETRIC_THRESHOLD(M1|M2) EVENT_TYPE(0-Normal|1-Alert)]"
	exit 1
fi

export PATH=/opt/local/libexec/gnubin:$PATH

_eventhub_ip=$1
_ma_number=$2
_st=$3
_hm=$4

simulators=(webpool tls openvas)
event_types=(security_metric)

if [ "${_st}" == "webpool" ];then
	_wp_metric=$5
	_wp_threshold=$6
	_wp_event_type=$7
fi

function generate_object_id() {
	local object_name=$1
	local object_uniq=`uuidgen`
	echo "${object_name}-${object_uniq:1:4}"
}

function monitoring_event() {
		local _component=$1
		local _object=$2
		local _labels=(${3})
		local _data=$4
		echo '{'
		echo "  \"component\":\"${_component}\","
		echo "  \"data\":\"${_data}\","
		echo '  "type" : "int",'
		echo "  \"object\" : \"${_object}\","
		echo -n "  \"labels\" : [ "
		_c=1
		for ((i=0;i<${#_labels[@]};i++));do
			echo -n "\"${_labels[$i]}\""
			if [ ${_c} -eq ${#_labels[@]} ];then
				_c=$((_c+1))
			else
				echo -n ","
				_c=$((_c+1))
			fi
		done
		#echo -n "\"${3}\""
		echo -n " ],"
		echo "  \"timestamp\": `date +%s`,"
		echo '  "token":null'
		echo '}'
}

_metric_value=
_labels=
_object_id=$(generate_object_id 'webpool')
function webpool_metric_generator() {
	if [ ${_wp_event_type} -eq 1 ];then
		_metric_value=$(shuf -n1 -i 0-$((_wp_threshold-1)))
		_labels=('security_metric' ${_wp_metric})
	else
		_metric_value=$(shuf -n1 -i ${_wp_threshold}-5)
		_labels=('security_metric' ${_wp_metric})
	fi
	monitoring_event 'webpool' ${_object_id} "${_labels[*]}" ${_metric_value}
}

for i in `seq 1 ${_hm}`;do
	webpool_metric_generator | curl -4 ${1}:9090/events/${_st} -H Content-Type:application/json -X POST --data-binary @/dev/stdin
	sleep $(shuf -n1 -e 1 2 3)
done
