var _st = {};
var timers = new Array();
var simulators_db = '{ \
  "testsm" : { \
    "name" : "TestSM", \
    "value" : "testsm", \
    "metrics" : {  \
      "specs_testsm_M3": { \
        "name": "specs_testsm_M3", \
        "label": "security_metric_M3_Cryptographic_Strength", \
        "value" : "8", \
        "operator" : "ge", \
        "slaid" : "SLA_ID_TEST" \
      }, \
      "specs_testsm_M4": { \
        "name": "specs_testsm_M4", \
        "label": "security_metric_M4_Forward_Secrecy", \
        "value" : "0", \
        "operator" : "ge", \
        "slaid" : "sla_id_TEST" \
      }, \
      "specs_testsm_M5": { \
        "name": "specs_testsm_M5", \
        "label": "security_metric_M5_HSTS", \
        "value" : "1", \
        "operator" : "ge", \
        "slaid" : "sla_id_TEST" \
      } \
    }\
  } \
}';
try {
  var simdb = JSON.parse(simulators_db);
} catch (e) {
  alert("ERROR: Simulators database is not in JSON format!");
}
var simno = 2;
var _alert_events_array = [];
var _alert_last_seq = 0;
var _alert_eventhub_id = '';
var alert_eventhub_timer;
var _secmetric_events_array = [];
var _secmetric_last_seq = 0;
var _secmetric_eventhub_id = '';
var secmetric_eventhub_timer;
var _all_events_array = [];
var _all_last_seq = 0;
var _all_eventhub_id = '';
var all_eventhub_timer;

function start_alert_eventhub(label_name) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/streams/alert/consume', true);
  if (_alert_events_array.length > 0) {
    xhr.setRequestHeader("Specs-Heka-Http-Gateway-Stream-Sequence", _alert_eventhub_id + ":" + (_alert_last_seq+1));
    console.log("xhr header: Specs-Heka-Http-Gateway-Stream-Sequence: " + _alert_eventhub_id + ":" + (_alert_last_seq+1));
  }
  xhr.send(null);
  var timer;
  alert_eventhub_timer = window.setInterval(function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      window.clearTimeout(alert_eventhub_timer);
      //$('body').append('done <br />');
      $('#notifications_events_switch').prop('checked', false);
    }
    //console.log("notifications: iteration ...");
    var _alert_events_list = xhr.responseText.split('\r');
    //var _last_new_seq = JSON.parse(_e[_e.length-2])['token']['seq']
    //console.log(_alert_events_list);
    if (_alert_events_list.length > 1) {
      var _events_list_last_seq = parseInt(JSON.parse(_alert_events_list[_alert_events_list.length-2])['token']['seq']);
      if ( _events_list_last_seq > _alert_last_seq) {
        for (i=0;i<_alert_events_list.length;i++) {
          if (_alert_events_list[i] != "") {
            try {
              var _ev = JSON.parse(_alert_events_list[i]);
            } catch (e) {
              console.log("error: not a valid json line");
            }
            if (parseInt(_ev['token']['seq']) > _alert_last_seq) {
              var dateString = timestamp_pretty_format(_ev['timestamp']);
              $('#eventhub_alert_table tr:first').after('<tr class="eventhub-alert-entry" seq="'+_ev['token']['seq']+'">'+
              ' <td>'+
              '   <b>'+_ev['token']['seq']+'</b>'+
              ' </td>'+
              ' <td>'+
              _ev['component']+
              ' </td>'+
              ' <td>'+
              _ev['object']+
              ' </td>'+
              ' <td>'+
              JSON.stringify(_ev['data'])+
              ' </td>'+
              ' <td>'+
              dateString+
              ' </td>'+
              '</tr>')
              $('#eventhub_alert_table tr:first').next().find('td').effect('highlight', {color: '#fcf8e3'}, 2000);
              _alert_events_array.push(_ev);
              _alert_last_seq = parseInt(_ev['token']['seq']);
            }
          }
        }
        if (_alert_eventhub_id == "") {
          _alert_eventhub_id = _alert_events_array[_alert_events_array.length-1]['token']['uuid'];
        }
      }
    }
    //console.log(_alert_eventhub_id);
    //console.log(_alert_last_seq);
    //console.log("alert: Stop iteration ...");
    //stop_eventhub();
    //xhr.abort();
  }, 1000);
}
function start_all_eventhub(label_name) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/streams/all/consume', true);
  if (_all_events_array.length > 0) {
    xhr.setRequestHeader("Specs-Heka-Http-Gateway-Stream-Sequence", _all_eventhub_id + ":" + (_all_last_seq+1));
    //console.log("xhr header: Specs-Heka-Http-Gateway-Stream-Sequence: " + _all_eventhub_id + ":" + (_all_last_seq+1));
  }
  xhr.send(null);
  var timer;
  all_eventhub_timer = window.setInterval(function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      window.clearTimeout(all_eventhub_timer);
      //$('body').append('done <br />');
      $('#all_events_switch').prop('checked', false);
    }
    //console.log("all: iteration ...");
    var _all_events_list = xhr.responseText.split('\r');
    //var _last_new_seq = JSON.parse(_e[_e.length-2])['token']['seq']
    //console.log(_all_events_list);
    if (_all_events_list.length > 1) {
      var _events_list_last_seq = parseInt(JSON.parse(_all_events_list[_all_events_list.length-2])['token']['seq']);
      if ( _events_list_last_seq > _all_last_seq) {
        for (i=0;i<_all_events_list.length;i++) {
          if (_all_events_list[i] != "") {
            try {
              var _ev = JSON.parse(_all_events_list[i]);
            } catch (e) {
              console.log("error: not a valid json line");
            }
            if (parseInt(_ev['token']['seq']) > _all_last_seq) {
              var dateString = timestamp_pretty_format(_ev['timestamp']);
              $('#eventhub_all_table tr:first').after('<tr class="eventhub-all-entry" seq="'+_ev['token']['seq']+'">'+
              ' <td>'+
              '   <b>'+_ev['token']['seq']+'</b>'+
              ' </td>'+
              ' <td>'+
              _ev['component']+
              ' </td>'+
              ' <td>'+
              _ev['object']+
              ' </td>'+
              ' <td>'+
              _ev['labels']+
              ' </td>'+
              ' <td>'+
              dateString+
              ' </td>'+
              '</tr>')
              $('#eventhub_all_table tr:first').next().find('td').effect('highlight', {color: '#d6e9c6'}, 2000);
              _all_events_array.push(_ev);
              _all_last_seq = parseInt(_ev['token']['seq']);
            }
          }
        }
        if (_all_eventhub_id == "") {
          _all_eventhub_id = _all_events_array[_all_events_array.length-1]['token']['uuid'];
        }
      }
    }
    //console.log(_all_eventhub_id);
    //console.log(_all_last_seq);
    //console.log("all: Stop iteration ...");
    //stop_eventhub();
    //xhr.abort();
  }, 1000);
}
function start_secmetric_eventhub(label_name) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/streams/securitymetric/consume', true);
  if (_secmetric_events_array.length > 0) {
    xhr.setRequestHeader("Specs-Heka-Http-Gateway-Stream-Sequence", _secmetric_eventhub_id + ":" + (_secmetric_last_seq+1));
    console.log("xhr header: Specs-Heka-Http-Gateway-Stream-Sequence: " + _secmetric_eventhub_id + ":" + (_secmetric_last_seq+1));
  }
  xhr.send(null);
  var timer;
  secmetric_eventhub_timer = window.setInterval(function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      window.clearTimeout(secmetric_eventhub_timer);
      //$('body').append('done <br />');
      $('#secmetric_events_switch').prop('checked', false);
    }
    //console.log("secmetric: iteration ...");
    var _secmetric_events_list = xhr.responseText.split('\r');
    //var _last_new_seq = JSON.parse(_e[_e.length-2])['token']['seq']
    //console.log(_secmetric_events_list);
    if (_secmetric_events_list.length > 1) {
      var _events_list_last_seq = parseInt(JSON.parse(_secmetric_events_list[_secmetric_events_list.length-2])['token']['seq']);
      if ( _events_list_last_seq > _secmetric_last_seq) {
        for (i=0;i<_secmetric_events_list.length;i++) {
          if (_secmetric_events_list[i] != "") {
            try {
              var _ev = JSON.parse(_secmetric_events_list[i]);
            } catch (e) {
              console.log("error: not a valid json line");
            }
            if (parseInt(_ev['token']['seq']) > _secmetric_last_seq) {
              var dateString = timestamp_pretty_format(_ev['timestamp']);
              $('#eventhub_secmetric_table tr:first').after('<tr class="eventhub-secmetric-entry" seq="'+_ev['token']['seq']+'">'+
              ' <td>'+
              '   <b>'+_ev['token']['seq']+'</b>'+
              ' </td>'+
              ' <td>'+
              _ev['component']+
              ' </td>'+
              ' <td>'+
              _ev['object']+
              ' </td>'+
              ' <td>'+
              _ev['data']+
              ' </td>'+
              ' <td>'+
              dateString+
              ' </td>'+
              '</tr>')
              $('#eventhub_secmetric_table tr:first').next().find('td').effect('highlight', {color: '#d9edf7'}, 2000);
              _secmetric_events_array.push(_ev);
              _secmetric_last_seq = parseInt(_ev['token']['seq']);
            }
          }
        }
        if (_secmetric_eventhub_id == "") {
          _secmetric_eventhub_id = _secmetric_events_array[_secmetric_events_array.length-1]['token']['uuid'];
        }
      }
    }
    //console.log(_secmetric_eventhub_id);
    //console.log(_secmetric_last_seq);
    //console.log("secmetric: Stop iteration ...");
    //stop_eventhub();
    //xhr.abort();
  }, 1000);
}
function timestamp_pretty_format(timestamp) {
  var date = new Date(timestamp * 1000);
  var year = date.getUTCFullYear();
  var month = date.getUTCMonth() + 1;
  var day = "-1";
  if (date.getUTCDate() < 10) {
    day = "0" + date.getUTCDate();
  } else {
    day = date.getUTCDate();
  }
  var hour = "-1";
  if (date.getUTCHours() < 10) {
    hour = "0" + date.getUTCHours();
  } else {
    hour = date.getUTCHours();
  }
  var minutes = "-1";
  if (date.getUTCMinutes() < 10) {
    minutes = "0" + date.getUTCMinutes();
  } else {
    minutes = date.getUTCMinutes();
  }
  var seconds = "-1";
  if (date.getUTCSeconds() < 10) {
    seconds = "0" + date.getUTCSeconds();
  } else {
    seconds = date.getUTCSeconds();
  }
  var date_string = year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
  return date_string;
}
function stop_eventhub() {
  window.clearTimeout(alert_eventhub_timer);
  window.clearTimeout(secmetric_eventhub_timer);
  window.clearTimeout(all_eventhub_timer);
}

$('#all_events_switch').on('click', function(){
  if ($(this).is(':checked')) {
    console.log("all_events_switch: start eventhub stream ...");
    start_all_eventhub();
  } else {
    console.log("all_events_switch: stop eventhub stream ...");
    window.clearTimeout(all_eventhub_timer);
  }
});

$('#secmetric_events_switch').on('click', function(){
  if ($(this).is(':checked')) {
    console.log("secmetric_events_switch: start eventhub stream ...");
    start_secmetric_eventhub();
  } else {
    console.log("secmetric_events_switch: stop eventhub stream ...");
    window.clearTimeout(secmetric_eventhub_timer);
  }
});

$('#notifications_events_switch').on('click', function(){
  if ($(this).is(':checked')) {
    console.log("notifications_events_switch: start eventhub stream ...");
    start_alert_eventhub();
  } else {
    console.log("notifictions_events_switch: stop eventhub stream ...");
    window.clearTimeout(alert_eventhub_timer);
  }
});

$('document').on('click', '#eventhub_alert_table', function(e){
  alert($(this));
  var _comp = $(this);
  console.log(_comp.attr('seq'));
});

var monipoli_rules_updated = 0;
$('#monipoli-tab').on('click', function(){
  monipoli_rules_update(1);
});

$('#monipoli_policy_upload').on('click', function(){
  if ($('#monipoli_policy_file').val() == '') {
    //$('#monipoli_modal_title').text('MoniPoli SLA Upload');
    //$('#monipoli_modal_body').html('<div class="alert alert-warning" role="alert">Select a new SLA document to be uploaded.</div>')
    //$('#monipoli_modal_trigger').trigger('click');
    $("#mp_warning").alert();
    $("#mp_warning").fadeTo(2000, 500).slideUp(500, function(){
      $("#mp_warning").hide();
    });
    return false;
  }
  console.log("monipoli: uploading a new SLA document ...");
  //var form_data = new FormData($('#monipoli_policy_upload_form')[0]);

  var _sla_doc = document.getElementById("monipoli_policy_file").files[0];
  var _fr = new FileReader();
  if(!window['oldXMLHttpRequest'].prototype.sendAsBinary){
    window['oldXMLHttpRequest'].prototype.sendAsBinary = function(datastr) {
      function byteValue(x) {
        return x.charCodeAt(0) & 0xff;
      }
      var ords = Array.prototype.map.call(datastr, byteValue);
      var ui8a = new Uint8Array(ords);
      this.send(ui8a.buffer);
      if (this.status != 200 && this.readyState == XMLHttpRequest.DONE) {
        //$('#monipoli_modal_title').text('MoniPoli SLA Upload');
        //$('#monipoli_modal_body').html('<div class="alert alert-danger" role="alert">New SLA document failed to upload.</div>')
        //$('#monipoli_modal_trigger').trigger('click');
        $("#mp_danger").alert();
        $("#mp_danger").fadeTo(3000, 500).slideUp(500, function(){
          $("#mp_danger").hide();
        });
        $('#monipoli_policy_upload_form').trigger('reset');
        $("#upload-file-info").html('');
      } else {
        //$('#monipoli_modal_title').text('MoniPoli SLA Upload');
        //$('#monipoli_modal_body').html('<div class="alert alert-success" role="alert">New SLA document has been successfully uploaded.</div>')
        //$('#monipoli_modal_trigger').trigger('click');
        $("#mp_success").alert();
        $("#mp_success").fadeTo(3000, 500).slideUp(500, function(){
          $("#mp_success").hide();
        });
        monipoli_rules_updated = 0;
        monipoli_rules_update(0);
        $('#monipoli_policy_upload_form').trigger('reset');
        $('#upload-file-info').html('');
      }
    }
  }
  var oxhr = window['oldXMLHttpRequest'];
  var xhr = new oxhr();
  xhr.open("POST", "/monipoli");
  xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
  _fr.onload = function(evt) {
    xhr.sendAsBinary(evt.target.result);
  }
  _fr.readAsBinaryString(_sla_doc);
});

function monipoli_rules_add_row(id, metric, operator, value, description,slaid) {
  $('#monipoli_rules').append('<tr class="monipoli-rules-entry" value="'+id+'" slaid="'+slaid+'">'+
  '<td>'+id+'</td>'+
  '<td>'+metric+'</td>'+
  '<td>'+operator+'</td>'+
  '<td>'+value+'</td>'+
  '<td>'+description+'</td>'+
  '</tr>');
}
function monipoli_rules_update(id) {
  if (monipoli_rules_updated == 1 && id != 0) {
    return;
  } else {
    if (id == 0) {
      $("#monipoli_rules").find("tr:gt(0)").remove();
    }
    $.ajax({
      url: '/monipoli',  //Server script to process data
      type: 'GET',
      success: function(data){
        var lines = data.split('\n');
        for (i=3;i<lines.length;i+=7) {
          //console.log(lines[i+1]);
          if ((lines[i] != " ") && (lines[i] != "") && (lines[i+1].trim() != "specs_dummy_M0")) {
            var _e = lines[i+1].split('_')[1].trim();
            if (!(_e in simdb)) {
              var _key = {}
              _key['name'] = lines[i+1].split('_')[1].toUpperCase().trim();
              _key['value'] = lines[i+1].split('_')[1].trim();
              _key['metrics'] = {};
              //console.log('new key: ' + _key['value'])
              simdb[_key['value']] = _key;
            }
            var _se = {};
            _se['name'] = lines[i+1].trim();
            _se['label'] = "security_metric_"+lines[i+2].trim();
            _se['value'] = lines[i+3].trim();
            _se['operator'] = lines[i+4].trim();
            _se['slaid'] = lines[i+5].trim();
            simdb[lines[i+1].split('_')[1]]['metrics'][_se['name']] = _se;
            //console.log("\nnew metric entry in simdb: \n\n" + JSON.stringify(simdb));
            monipoli_rules_add_row(lines[i].replace(':', ''), lines[i+1], lines[i+4], lines[i+3], lines[i+2], lines[i+5]);
          }
        }
        //console.log(lines);
        monipoli_rules_updated = 1;
      },
      error: function(){
        $('#monipoli_modal_title').text('MoniPoli Rules Update');
        $('#monipoli_modal_body').html('<div class="alert alert-danger" role="alert">MoniPoli Rules Service is unresponsive!!</div>')
        $('#monipoli_modal_trigger').trigger('click');
      },
      //cache: false,
      contentType: false,
      processData: false
    });
  }
}

$('#simulators-tab').on('click', function(){
    simulator_update_fields("0");
});
function simulator_update_fields(field_id) {
  if (field_id == "0") {
    $('.simulator-ma-type').each(function(){
      //console.log($(this).val());
      if ($(this).val() == '' || $(this).val() == null) {
        $(this).find('option').remove().end();
        var _trigger = 0;
        for (var k in simdb) {
          $(this).append('<option value="'+simdb[k]['value']+'">'+simdb[k]['name']+'</option>');
          if (_trigger == 0) {
            $(this).val(simdb[k]['value']).change();
            _trigger = 1;
          }
        }
      } else {
        //console.log($('option', this).size() + " " + Object.keys(simdb).length);
        if ($('option', this).size() < Object.keys(simdb).length) {
          for (k in simdb) {
            if ($("option[value='" + k + "']", this).length === 0) {
              $(this).append('<option value="'+simdb[k]['value']+'">'+simdb[k]['name']+'</option>');
            }
          }
        }
      }
    });
  } else {
    $('.simulator-ma-type').each(function(){
      if ($(this).attr('svalue') == field_id) {
        $(this).find('option').remove().end();
        var _trigger = 0;
        for (var k in simdb) {
          $(this).append('<option value="'+simdb[k]['value']+'">'+simdb[k]['name']+'</option>');
          if (_trigger == 0) {
            $(this).val(simdb[k]['value']).change();
            _trigger = 1;
          }
        }
      }
    });
  }

}
$(document).on('change', '.simulator-ma-type', function(){
  var _id = $(this).attr('svalue');
  var _ma_type = $(this).val();
  $('.simulator-ma-secmetric').each(function(){
    if ($(this).attr('svalue') == _id) {
      $(this).find('option').remove().end();
      for (var v in simdb[_ma_type]['metrics']) {
        $(this).append('<option sm_slaid="'+simdb[_ma_type]['metrics'][v]['slaid']+'" sm_value="'+simdb[_ma_type]['metrics'][v]['value']+'" sm_operator="'+simdb[_ma_type]['metrics'][v]['operator']+'" value="'+simdb[_ma_type]['metrics'][v]['name']+'">'+simdb[_ma_type]['metrics'][v]['name']+'</option>');
      }
    }
  });
});

$(document).on('click', '#simulator_close', function(){
  var _id = $(this).attr('svalue');
  destroy_simulator(_id);
  $(this).parent().parent().parent().parent().remove();
});
$('#simulator_add').on('click', function(){
  $('#simulators-content-list').append(simulator_panel_add(simno));
  simulator_update_fields(simno);
  simno++;
});

function simulator_panel_add(sim_no) {
  var simulator_content = '\
  <div id="simulator_'+sim_no+'" class="col-md-12" value="'+sim_no+'"> \
  <div class="panel panel-success"> \
  <div class="panel-heading"> \
  <h3 class="panel-title" style="text-align: center;">Simulator #'+sim_no+' <a class="close" style="color:red;" svalue="'+sim_no+'" id="simulator_close">X</a></h3> \
  </div> \
  <div class="panel-body"> \
  <div class="col-md-2"> \
  Simulator type \
  </div> \
  <div class="col-md-3"> \
  Security metric \
  </div> \
  <!-- \
  <div class="col-md-2"> \
  Threshold \
  </div> \
  --> \
  <div class="col-md-2"> \
  Event Type \
  </div> \
  <div class="col-md-4"> \
  No. of events to generate \
  </div> \
  <div class="col-md-1"> \
  </div> \
  <!-- monitoring agent type button --> \
  <div class="col-md-2"> \
  <select class="form-control simulator-ma-type" id="simulator_ma_type_'+sim_no+'" svalue="'+sim_no+'"> \
  <option value=""></option> \
  </select> \
  </div> \
  <!-- monitoring agent secury metric --> \
  <div class="col-md-3"> \
  <select class="form-control simulator-ma-secmetric" id="simulator_ma_secmetric_'+sim_no+'" svalue="'+sim_no+'"> \
  <option value=""></option> \
  </select> \
  </div> \
  <!-- monitoring agent secury metric threshold \
  <div class="col-md-2"> \
  <input type="text" class="form-control simulator-ma-secmetric-threshold" placeholder="Value ..." id="simulator_ma_secmetric_threshold_'+sim_no+'" svalue="'+sim_no+'"> \
  </div> \
  --> \
  <!-- monitoring agent event type --> \
  <div class="col-md-2"> \
  <select class="form-control simulator-ma-event-type" id="simulator_ma_event_type_'+sim_no+'" svalue="'+sim_no+'"> \
  <option value="normal">normal</option> \
  <option value="alert">alert</option> \
  </select> \
  </div> \
  <!-- monitoring agent secury number of events --> \
  <div class="col-md-4"> \
  <input type="text" class="form-control simulator-ma-event-number" placeholder="How many ..." id="simulator_ma_event_number_'+sim_no+'" svalue="'+sim_no+'"> \
  </div> \
  <!-- ON/OFF Switch --> \
  <div class="col-md-1" style="float:left;vertical-alignment:center;"> \
  <div class="onoffswitch"> \
  <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox simulator-ma-switch" id="simulator_switch_'+sim_no+'" svalue="'+sim_no+'"> \
  <label class="onoffswitch-label" for="simulator_switch_'+sim_no+'"> \
  <span class="onoffswitch-inner"></span> \
  <span class="onoffswitch-switch"></span> \
  </label> \
  </div> \
  </div> \
  <!-- ./ ON/OFF Switch --> \
  </div> \
  </div> \
  </div> \
  ';
  return simulator_content;
}

function generate_UUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random()*16)%16 | 0;
    d = Math.floor(d/16);
    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });
  return uuid;
};

function generate_object_id (object_name) {
  return object_name+'-'+generate_UUID().substring(0,4);
}

function generate_timestamp() {
  return Math.floor(Date.now() / 1000);
}
function generate_random(min, max, current)
{
  if (current == -1) {
    return Math.floor(Math.random()*(max - min + 1)) + min;
  } else {
    var _val = Math.floor(Math.random()*(max - min + 1)) + min;
    while ( _val == current) {
      _val = Math.floor(Math.random()*(max - min + 1)) + min;
    }
    return _val;
  }
}

function monitoring_event(component_id, data, type, object_id, labels, timestamp) {
  var _event = {};
  _event['component'] = component_id;
  _event['data'] = data;
  _event['type'] = type;
  _event['object'] = object_id;
  _event['labels'] = labels;
  _event['timestamp'] = timestamp;
  _event['token'] = null;
  return _event
}
function destroy_simulator(id) {
  var _id = id;
    console.log('timer['+_id+']: stopped ...');
    _st[_id]['counter'] = 0;
    _st[_id]['how_many'] = 0;
    $('#simulator_switch_'+_id).prop('checked', false);
    window.clearTimeout(timers[_id]);
    _st[_id]['status'] = 0;
}
function send_event(id, sim_type, sec_metric, object_id, sla_id, operator, threshold, event_type, how_many) {
  var _data = -999;
  if (event_type == "alert") {
    // added support for true/false string values!!!
    if (!isNaN(threshold)) {
      if (operator == 'eq') {
        _data = generate_random(1, (threshold+5), threshold);
      }
      if (operator == 'lt' || operator == 'le') {
        _data = generate_random(threshold+1, threshold+10, -1);
      }
      if (operator == 'gt' || operator == 'ge') {
        _data = generate_random(0, threshold-1, -1);
      }
    } else {
      if (operator == 'eq') {
        if (threshold == 'false') {
          _data = 'true';
        } else {
          _data = 'false';
        }
      }
    }
  } else {
    if (!isNaN(threshold)) {
      if (operator == 'eq') {
        _data = threshold;
      }
      if (operator == 'lt' || operator == 'le') {
        _data = generate_random(0, threshold-1, -1);
      }
      if (operator == 'gt' || operator == 'ge') {
        _data = generate_random(threshold, threshold+3, -1);
      }
    } else {
      if (operator == 'eq') {
        _data = threshold;
      }
    }
  }
  console.log('timer['+id+'] operator ['+operator+'] value ['+threshold+'] data sent ['+_data+'] -(POST)> /events/'+ sim_type);
  var _call = $.ajax({
    url: '/events/'+sim_type,  //Server script to process data
    type: 'POST',
    data: JSON.stringify(monitoring_event(sim_type, String(_data), "int", object_id, ['sla_id_'+sla_id, simdb[sim_type]['metrics'][sec_metric]['label'], generate_object_id('job')], generate_timestamp()), null, '\t'),
    success: function(data){
    },
    error: function(){
      console.log("send_event: ERROR");
      destroy_simulator(id);
    }
  });
  return _call;
}
function get_object_value(object_name, id) {
  var _value = '';
  $('.'+object_name).each(function(){
    if ($(this).attr('svalue') == id) {
      _value = $(this).val();
      return false;
    }
  });
  return _value;
}
function get_object_value_attribute(object_name, attribute_name, id) {
  var _value = '';
  $('.'+object_name).each(function(){
    if ($(this).attr('svalue') == id) {
      _value = $('option:selected', this).attr(attribute_name);
      return false;
    }
  });
  return _value;
}

$(document).on('click', '.simulator-ma-switch', function(event){
  var _id = parseInt($(this).attr('svalue'));
  var _sim_type = get_object_value('simulator-ma-type', _id);
  var _sec_metric = get_object_value('simulator-ma-secmetric', _id);
  var _threshold = get_object_value_attribute('simulator-ma-secmetric', 'sm_value', _id);
  var _sla_id = get_object_value_attribute('simulator-ma-secmetric', 'sm_slaid', _id);
  if (_threshold == '' || typeof _threshold === 'undefined') {
    _threshold = 0;
    return false;
  } else {
    if (!isNaN(_threshold)) {
      _threshold = parseInt(_threshold);
    }
  }
  var _operator = get_object_value_attribute('simulator-ma-secmetric', 'sm_operator', _id);
  if (_operator == '' || typeof _operator === 'undefined') {
    _operator = '';
    return false;
  }
  //console.log(_threshold);
  var _event_type = get_object_value('simulator-ma-event-type', _id);
  //console.log(_event_type);
  var _how_many = get_object_value('simulator-ma-event-number', _id);
  if (_how_many == '' || typeof _how_many === 'undefined') {
    _how_many = 0;
    $('#simulator_switch_'+_id).prop('checked', false);
    $('#simulator_ma_event_number_'+_id).focus();
    return false;
  } else {
    _how_many = parseInt(_how_many);
  }
  // simulator timers array
  _st[_id] = {id: _id, status: 0, sim_type: _sim_type, sec_metric : _sec_metric, operator: _operator, threshold : _threshold, event_type : _event_type, how_many : _how_many, counter: 0, sla_id: _sla_id };
  if ($(this).is(':checked')) {
    var rand = generate_random(1000, 3000, -1);
    _st[_id]['status'] = 1;
    console.log('timer['+_id+'] ['+rand+' ms] started ...');
    timers[_id] = setInterval(function() {
      if (_st[_id]['counter'] > (_st[_id]['how_many']-1)) {
          destroy_simulator(_id);
      } else {
            var object_id = generate_object_id(_sim_type);
            console.log('timer['+_id+'] ['+rand+' ms] counter ['+(_st[_id]['counter']+1)+'] how_many ['+_st[_id]['how_many']+'] event_type ['+_st[_id]['event_type']+']');
            send_event(_id, _st[_id]['sim_type'], _st[_id]['sec_metric'], object_id, _st[_id]['sla_id'], _st[_id]['operator'], _st[_id]['threshold'], _st[_id]['event_type'], _st[_id]['how_many']);
            _st[_id]['counter']++;
      }
    }, rand);
  } else {
    destroy_simulator(_id);
  }
});

$(function(){
  monipoli_rules_update(1);
  //$("#mp_danger").hide();
  //$("#mp_success").hide();
  //$("#mp_warning").hide();
})
